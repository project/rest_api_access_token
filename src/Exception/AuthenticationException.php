<?php

namespace Drupal\rest_api_access_token\Exception;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class AuthenticationException.
 *
 * @package Drupal\rest_api_access_token\Exception
 */
class AuthenticationException extends BadRequestHttpException {

}
