<?php

namespace Drupal\rest_api_access_token\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class EntityNotFoundException.
 *
 * @package Drupal\rest_api_access_token\Exception
 */
class EntityNotFoundException extends NotFoundHttpException {

}
