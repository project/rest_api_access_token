<?php

namespace Drupal\rest_api_access_token\Exception;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class InvalidRequestIdException.
 *
 * @package Drupal\rest_api_access_token\Exception
 */
class InvalidRequestIdException extends BadRequestHttpException {

}
