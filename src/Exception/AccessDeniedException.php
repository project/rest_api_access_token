<?php

namespace Drupal\rest_api_access_token\Exception;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class AccessDeniedException.
 *
 * @package Drupal\rest_api_access_token\Exception
 */
class AccessDeniedException extends AccessDeniedHttpException {

}
