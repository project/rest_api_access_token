<?php

namespace Drupal\rest_api_access_token\Service;

use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest_api_access_token\Exception\AuthenticationException;
use Drupal\rest_api_access_token\Exception\TokenGeneratorException;
use Drupal\rest_api_access_token\Exception\TokenNotFoundException;
use Drupal\rest_api_access_token\Repository\TokenRepository;
use Drupal\user\Entity\User;
use Drupal\user\UserAuthInterface;

/**
 * Class LoginService.
 *
 * @package Drupal\rest_api_access_token\Service
 */
class LoginService {

  /**
   * Token generator service.
   *
   * @var \Drupal\rest_api_access_token\Service\TokenGenerator
   */
  protected $tokenGenerator;

  /**
   * Token model repository.
   *
   * @var \Drupal\rest_api_access_token\Repository\TokenRepository
   */
  protected $tokenRepository;

  /**
   * Entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * User authentication service.
   *
   * @var \Drupal\user\UserAuthInterface
   */
  protected $userAuth;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * LoginService constructor.
   *
   * @param \Drupal\rest_api_access_token\Service\TokenGenerator $tokenGenerator
   *   Token generator service.
   * @param \Drupal\rest_api_access_token\Repository\TokenRepository $tokenRepository
   *   Token model repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity manager service.
   * @param \Drupal\user\UserAuthInterface $userAuth
   *   User authentication service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *    Config factory.
   */
  public function __construct(TokenGenerator $tokenGenerator, TokenRepository $tokenRepository, EntityTypeManagerInterface $entityTypeManager, UserAuthInterface $userAuth, ConfigFactoryInterface $configFactory) {
    $this->tokenGenerator = $tokenGenerator;
    $this->tokenRepository = $tokenRepository;
    $this->entityTypeManager = $entityTypeManager;
    $this->userAuth = $userAuth;
    $this->configFactory = $configFactory;
  }

  /**
   * User login.
   *
   * @param string $login
   *   User email or username.
   * @param string $password
   *   User password.
   *
   * @return bool|\Drupal\rest_api_access_token\Model\Token
   *   Token model
   *
   * @throws \Exception
   *   Invalid user login or password.
   */
  public function login(string $login, string $password) {
    if (empty($login) || empty($password)) {
      throw new AuthenticationException("Invalid user login or password");
    }
    $user = FALSE;

    $loginByMail = (bool) $this->configFactory->get('rest_api_access_token.config')
      ->get('login_by_mail');
    $loginByName = (bool) $this->configFactory->get('rest_api_access_token.config')
      ->get('login_by_name');

    // Login by name.
    if ($loginByName) {
      $users = $this->entityTypeManager->getStorage('user')
        ->loadByProperties(['name' => $login]);
      $user = reset($users);
    }

    // login by mail
    if (!$user && $loginByMail) {
      $users = Drupal::entityTypeManager()->getStorage('user')
        ->loadByProperties(['mail' => $login]);

      /** @var User $user */
      $user = reset($users);
    }

    if ($user) {
      $login = $user->getAccountName();
    }
    else {
      throw new AuthenticationException("Invalid user login or password");
    }

    // Authenticate user.
    $uid = (int) $this->userAuth->authenticate($login, $password);
    if ($uid <= 0) {
      throw new AuthenticationException("Invalid user login or password");
    }

    $token = FALSE;
    try {
      for ($i = 0; $i < 5; $i++) {
        $token = $this->tokenGenerator->execute($uid);
        $this->tokenRepository->getByPublicToken($token->getPublic());
        $token = FALSE;
      }
    }
    catch (TokenNotFoundException $exception) {
    }

    if (empty($token)) {
      throw new TokenGeneratorException("Error during generate token");
    }

    $this->tokenRepository->insert($token);

    /**
     * If you want to remove other user tokens, please use EventSubscriberInterface and AccessTokenEvents::TOKEN_RESPONSE
     * and use the following script:
     * try {
     *    $this->tokenRepository->removeOtherUserTokens($token);
     * } catch (\Throwable $e) {}
     */
    return $token;
  }

  /**
   * Logout user form current device.
   *
   * @param string $publicToken
   *   Public user auth token.
   *
   * @return bool
   *   Logout result.
   */
  public function logout(string $publicToken) {
    return (bool) $this->tokenRepository->removeByPublicToken($publicToken);
  }

  /**
   * Logout user form all devices.
   *
   * @param \Drupal\user\Entity\User $user
   *   User entity.
   *
   * @return bool
   *   Logout result.
   */
  public function logoutFromAllDevices(User $user) {
    return (bool) $this->tokenRepository->removeByUser($user);
  }

}
