<?php

namespace Drupal\rest_api_access_token\Authentication\Event;

/**
 * Class AccessTokenEvents with event names.
 *
 * @package Drupal\rest_api_access_token\Authentication\Event
 */
final class AccessTokenEvents {

  const TOKEN_RESPONSE = 'rest_api_access_token.token_response';

  const LOGOUT = 'rest_api_access_token.logout';

  const LOGOUT_FROM_ALL_DEVICES = 'rest_api_access_token.logout_from_all_devices';

}
