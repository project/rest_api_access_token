<?php

namespace Drupal\rest_api_access_token\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 *
 * @package Drupal\rest_api_access_token\Form
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'rest_api_access_token.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rest_api_access_token_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="rest_api_access_token_config_form">',
      '#suffix' => '</div>',
    ];

    $form['container']['login'] = [
      '#type' => 'fieldset',
      '#title' => t('API login options'),
    ];
    $form['container']['login']['login_by_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Login via API by user name (username).'),
      '#description' => $this->t('Using API endpoint: "/auth/login"'),
      '#default_value' => $this->config('rest_api_access_token.config')
        ->get('login_by_name'),
    ];
    $form['container']['login']['login_by_mail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Login via API by user mail.'),
      '#description' => $this->t('Using API endpoint: "/auth/login"'),
      '#default_value' => $this->config('rest_api_access_token.config')
        ->get('login_by_mail'),
    ];

    $form['container']['cache'] = [
      '#type' => 'fieldset',
      '#title' => t('API response cache options'),
    ];

    $form['container']['cache']['cache_endpoints'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable cache endpoints by REQUEST-ID (in header).'),
      '#default_value' => $this->config('rest_api_access_token.config')
        ->get('cache_endpoints'),
      '#description' => $this->t('Required REQUEST-ID if cache enabled.'),
    ];

    $form['container']['cache']['cache_endpoints_lifetime'] = [
      '#type' => 'number',
      '#title' => $this->t('Set lifetime of cache endpoints in seconds.'),
      '#default_value' => (int) $this->config('rest_api_access_token.config')
        ->get('cache_endpoints_lifetime'),
      '#description' => $this->t('For CACHE_PERMANENT set 0. For disable cache set -1.'),
    ];

    $form['container']['more'] = [
      '#type' => 'fieldset',
      '#title' => t('More API options'),
    ];
    $form['container']['more']['signature_verification'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('X-AUTH-SIGNATURE HEADER: sha256("token|requestId|path|base64_body|secret")'),
      '#title' => $this->t('Enable signature verification.'),
      '#default_value' => $this->config('rest_api_access_token.config')
        ->get('signature_verification'),
    ];
    $form['container']['more']['token_lifetime_hours'] = [
      '#type' => 'number',
      '#title' => $this->t('Set lifetime of auth token in hours.'),
      '#default_value' => (int) $this->config('rest_api_access_token.config')
        ->get('token_lifetime_hours'),
      '#description' => $this->t('For infinite set 0.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $loginByName = $form_state->getValue('login_by_name');
    $loginByMail = $form_state->getValue('login_by_mail');
    if (!$loginByName && !$loginByMail) {
      $form_state->setErrorByName('login_by_name', $this->t('Please, select minimum one login method.'));
      $form_state->setErrorByName('login_by_mail', $this->t('Please, select minimum one login method.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();

    if (isset($values['signature_verification'])) {
      $this->config('rest_api_access_token.config')
        ->set('signature_verification', $values['signature_verification'])
        ->save();
    }

    if (isset($values['cache_endpoints'])) {
      $this->config('rest_api_access_token.config')
        ->set('cache_endpoints', $values['cache_endpoints'])
        ->save();
    }

    if (isset($values['cache_endpoints_lifetime'])) {
      $this->config('rest_api_access_token.config')
        ->set('cache_endpoints_lifetime', $values['cache_endpoints_lifetime'])
        ->save();
    }

    if (isset($values['token_lifetime_hours'])) {
      $this->config('rest_api_access_token.config')
        ->set('token_lifetime_hours', $values['token_lifetime_hours'])
        ->save();
    }

    if (isset($values['login_by_name'])) {
      $this->config('rest_api_access_token.config')
        ->set('login_by_name', $values['login_by_name'])
        ->save();
    }

    if (isset($values['login_by_mail'])) {
      $this->config('rest_api_access_token.config')
        ->set('login_by_mail', $values['login_by_mail'])
        ->save();
    }
  }

}
